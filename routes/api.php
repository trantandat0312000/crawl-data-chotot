<?php

use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix("/v1")->group(function($router){
    $router->prefix("/product")->group(function($productRouter){
        $productRouter->get('/',[ProductController::class,'list'])->name('apiGetListProductDb');
        $productRouter->get('/get-list', [ProductController::class, 'getListProductChotot'])->name('apiGetListProductChoTot');
        $productRouter->get('/details', [ProductController::class, 'getDetalProduct'])->name('getDetalProduct');
    });
});
