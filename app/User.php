<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class User extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'users';

    protected $fillable = ['name','phone','password'];
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected $dateFormat = 'U';
}
