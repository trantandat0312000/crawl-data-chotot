<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Product extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'products';
    protected $fillable = [
        'ad_id',
        'list_id',
        'list_time',
        'date',
        'account_id',
        'account_oid',
        'account_name',
        'shop_alias',
        'subject',
        'body',
        'category',
        'category_name',
        'area',
        'area_name',
        'region',
        'region_name',
        'company_ad',
        'condition_ad',
        'type',
        'price',
        'price_string',
        'videos',
        'special_display_images',
        'special_display',
        'number_of_images',
        'mfdate',
        'shop',
        'alias',
        'status',
        'name',
        'address',
        'profileImageUrl',
        'createdDate',
        'modifiedDate',
        'urls',
        'avatar',
        'region_v2',
        'area_v2',
        'ward',
        'ward_name',
        'carmodel',
        'carbrand',
        'carmodel_name',
        'carseats',
        'condition_ad_name',
        'mileage',
        'mileage_v2',
        'gearbox',
        'contain_videos',
        'location',
        'longitude',
        'latitude',
        'phone_hidden',
        'fuel',
        'cartype',
        'owner',
        'user',
        'protection_entitlement',
        'escrow_can_deposit',
        'escrow_config_id',
        'params',
        'veh_inspection_enable',
        'veh_inspected'
        ];
}
