<?php

namespace App\Scraper;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class chototScaper
{
    private $productId;
    private $relativePath;
    private $domain;
    public function __construct($productId, $relativePath)
    {
        $this->productId = $productId;
        $this->relativePath = $relativePath;
        $this->domain = config('chotot.domain', 'https://xe.chotot.com');
    }

    public function scrape()
    {
        $url = $this->domain . '/' . $this->relativePath;

        $client = new Client();

        $crawler = $client->request('GET', $url);
        $phoneElm = $crawler->filter('div.IntersectBox  div.sc-ifAKCX')->first();
        dd($phoneElm);
    }
}
