<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Store extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'stores';

    protected $fillable = [
        'alias', 
        'status', 
        'name', 
        'address', 
        'profileImage', 
        'creadtedDate', 
        'modifiedDate', 
        'urls', 
        'shopsCategoriesRelationships',
        'operatingTime',
        'numberOfFollowers',
        'numberOfItemsSold',
        'phone',
        'description',
    ];

}
