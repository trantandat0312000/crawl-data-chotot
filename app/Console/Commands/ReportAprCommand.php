<?php

namespace App\Console\Commands;

use App\Product;
use App\ProductCategory;
use App\ProductImage;
use App\Store;
use App\User;
use Facade\FlareClient\Http\Client;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Cookie;
use Illuminate\Console\Command;
use Facebook\WebDriver\Firefox\FirefoxOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use stdClass;

class ReportAprCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $domain;
    private $urlShop;
    protected $signature = 'report:apr {offset}';
    private $title;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Example connect selenium with chrome driver';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->domain = config('chotot.domain', 'https://xe.chotot.com');
        $this->urlShop = 'https://xe.chotot.com/cua-hang/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            $offset = $this->argument('offset');
            $response = $this->getListProduct($offset);
            $products = $response['ads'];
            $index = 0;
            foreach ($products as $key => $value) {
                $productExs = Product::where('list_id', '=', $value['list_id'])->take(1)->get();
                $countProductExs = $productExs->count();
                if ($countProductExs > 0) {
                    dump($value['list_id']);
                    continue;
                }
                if (isset($value['location']) && !is_null($value['location'])) {
                    $value['location'] = ['type' => 'Point', 'coordinates' => [$value['longitude'], $value['latitude']]];
                }
                $productId = $value['list_id'];
                $locationStr = $value['area_name'] . ' ' . $value['region_name'];
                $relativePath = $this->createSlug($locationStr) . '/' . $productId . '.htm';
                $fullPath = $this->domain . '/mua-ban-oto-' . $relativePath;
                $request = Http::get($fullPath);
                if (count($value['videos']) > 0) {
                    Log::channel('products_info')->info($fullPath . 'has ' . count($value['videos']) . ' video');
                    dump($fullPath);
                    continue;
                }
                if ($request->status() !== 200) {
                    Log::channel('products_info')->info($fullPath . ' sold out');
                    dump($fullPath);
                    continue;
                }

                $product = new Product();
                $product->fill($value);
                $host = 'http://localhost:9515';
                $desiredCapabilities = DesiredCapabilities::chrome();
                $totalDetailImg = $product['number_of_images'];

                // Disable accepting SSL certificates
                $desiredCapabilities->setCapability('acceptSslCerts', false);
                $firefoxOptions = new FirefoxOptions();
                $firefoxOptions->addArguments(['-headless']);
                $desiredCapabilities->setCapability(FirefoxOptions::CAPABILITY, $firefoxOptions);
                $driver = RemoteWebDriver::create($host, $desiredCapabilities);
                $driver->manage()->window()->maximize();
                $store = new Store();

                $driver->get($fullPath);
                if ($product['number_of_images'] > 1) {
                    try {
                        if (!isset($value['special_display_images'])) {
                            $image = [];
                            for ($i = 1; $i <= $totalDetailImg - 1; $i++) {
                                $xpath = '//*[@id="__next"]/div/div[3]/div[1]/div/div[4]/div/div[1]/div/div[1]/div[2]/div/div/div/div[' . $i . ']/div/div/span/img';
                                $detailImgElement = $driver->findElement(WebDriverBy::xpath($xpath))->getAttribute("src");
                                $countClickNext = (int) $totalDetailImg / 5;
                                if ($i <= $totalDetailImg && $totalDetailImg > 5) {
                                    $clickButtonNextImg = $driver->findElement(WebDriverBy::xpath('//*[@id="__next"]/div/div[3]/div[1]/div/div[4]/div/div[1]/div/div[1]/div[2]/div/button[2]'));
                                    $clickButtonNextImg->click();
                                    sleep(1);
                                }
                                $image[] = $detailImgElement;
                            }
                            $product['special_display_images'] = $image;
                        }
                    } catch (\Exception $e) {
                        dd($e);
                    }
                }
                $product['special_display_images_src'] = $value['special_display_images'] ?? null;
                $product->source_url = $fullPath;
                $phone = $this->getPhone($driver);
                if (!$phone) {
                    $driver->quit();
                    continue;
                }
                $product->params = $this->getOptionsProduct($driver, (string) $value['category']);

                $data_pImag = array(
                    'key' => $fullPath,
                    'category' => $this->createSlug($product['category_name'], '-'),
                    'title' => $product['category'] == '2030' ? 'phu-tung' : $this->createSlug($this->title,'-'),
                    'images' => json_encode($product['special_display_images']),
                );
                dump($data_pImag);
                $data_pImg_string = http_build_query($data_pImag);
                $outputDecode = $this->uploadImageServer($data_pImg_string);
                if (isset($outputDecode->response)) {
                    $product['special_display_images'] = $outputDecode->response;
                }
                //check avatar
                $responseImgAvatar = Http::timeout(60)->get($value['avatar']);
                $userDb = User::where('phone', '=', $phone)->take(1)->get();
                $user = new User();
                $user->name = $value['account_name'];
                $user->password = null;
                $user->phone = $phone;
                $user->account_id = $value['account_id'];
                $user->account_oid = $value['account_oid'];
                if ($responseImgAvatar->successful()) {
                    $data_avatar = array(
                        'member_id' => $value['account_id'],
                        'link' => $value['avatar']
                    );
                    $data_avatar_string = http_build_query($data_avatar);
                    $outputDecodeAvatar = $this->uploadImageServer($data_avatar_string, true);
                }
                if ($userDb->count() == 0) {
                    if (isset($outputDecodeAvatar) && isset($outputDecodeAvatar->response)) {
                        unset($outputDecodeAvatar->response->message);
                        $user->avatar = $outputDecodeAvatar->response;
                    } else {
                        $user->avatar = $value['avatar'];
                    }
                    $user->save();
                }

                $object = new \stdClass;

                $object->name = $user->name;
                $object->phone = $user->phone;
                $object->account_id = $value['account_id'];
                $object->account_oid = $value['account_oid'];
                if (isset($outputDecodeAvatar->response)) {
                    unset($outputDecodeAvatar->response->message);
                    $object->avatar = $outputDecodeAvatar->response;
                }
                $product->user = $object;
                $product->view = 1;
                $product->save();
                dump($value);
                if (isset($value['shop'])) {
                    $request = Http::get($this->urlShop . $value['shop']['urls'][0]['url']);

                    if ($request->status() == 200) {
                        $checkStore = Store::where('alias', '=', $value['shop']['alias'])->first();
                        if (!$checkStore) {
                            $driver->get($this->urlShop . $value['shop']['urls'][0]['url']);
                            $store->fill($value['shop']);
                            $store->phone = $phone;
                            $morDescription = $driver->findElement(WebDriverBy::xpath('//*[@id="__next"]/main/div[2]/div[5]/div/div/button'));
                            if ($morDescription) {
                                $morDescription->click();
                            }
                            $store->description = $driver->findElement(WebDriverBy::xpath('//*[@id="__next"]/main/div[2]/div[5]/div/div/div'))->getText();
                            $data_pImag = array(
                                'key' => $this->urlShop . $value['shop']['urls'][0]['url'],
                                'category' => 'avatar-store',
                                'title' => 'avatar-store',
                                'images' => json_encode(array($value['shop']['profileImageUrl'])),
                            );
                            dump($data_pImag);
                            $data_pImg_string = http_build_query($data_pImag);
                            $outputDecode = $this->uploadImageServer($data_pImg_string);
                            if (isset($outputDecode->response)) {
                                $store->profileImage = $outputDecode->response;
                            }
                            $store->save();
                        }
                    }
                }
                $driver->quit();

                Log::channel('products_info')->info($product->list_id . ' checked');
            }
            $offset += 100;
            $productstr = "'" . '' . "'";
            if ($offset == 1000) {
                dd($phone);
            }
            Artisan::call('report:apr', [
                'offset' => $offset,
            ]);
            return 0;
        } catch (\Exception $e) {
            dump("loi");
            dd($e);
            return 0;
        }
    }


    public function uploadImageServer($data_string, $isAvatar = false)
    {
        $url = $isAvatar ? 'http://222.255.238.136:3030/v1/crawler-avatar' : 'http://222.255.238.136:3030/crawler-images';
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            $data_string
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
        ));
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        $outputDecode = json_decode($server_output);

        curl_close($ch);
        return $outputDecode;
    }

    public function getPhone($driver)
    {
        try {
            $driver->findElement(WebDriverBy::tagName('linkcontact')) // find search input element
                ->click(); // fill the search box
            $driver->wait(500, 1000);

            $phoneParentEle = $driver->findElements(WebDriverBy::tagName('linkcontact'));
            $popup = $driver->findElements(WebDriverBy::className('m1ujxhqg'));
            if (count($popup) > 0) {
                $driver->findElement(WebDriverBy::xpath('/html/body/div[11]/div[2]/div[2]/div[3]/form/div[2]/button[2]'))->click();
                $driver->findElement(WebDriverBy::tagName('linkcontact')) // find search input element
                    ->click();
            }
            $phone = $phoneParentEle[0]->findElement(WebDriverBy::tagName("strong"))->getText();
            return $phone;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getOptionsProduct($driver, string $category)
    {
        $productOptions = [];
        $typeCate = config('xedy.category_product_by_id_crawler')[$category];
        $isMore = $driver->findElements(WebDriverBy::className('styles_button__SVZnw'));
        if ($isMore) {
            $driver->findElements(WebDriverBy::className('styles_button__SVZnw'))[0]->click();
            sleep(1);
        }
        $allOptions = $driver->findElements(WebDriverBy::className('AdParam_adMediaParam__3epxo'));
        $brandId = 0;
        foreach ($allOptions as $key => $value) {
            $options = $value->findElements(WebDriverBy::xpath(".//div[2]/span/*[local-name()='a' or local-name()='span']"));
            foreach ($options as $keyOptions => $valueOption) {
                $detail = new stdClass;
                if ($keyOptions % 2 == 0) {
                    $detail->label = str_replace(":", "", $valueOption->getText());
                    $detail->id = config('xedy.convert_label_to_variable')[$typeCate][$detail->label];
                    $productOptions[] = $detail;
                } else {
                    $valueOptionProduct = $valueOption->getText();
                    $proCate = ProductCategory::where('type', '=', $typeCate)->first();
                    $proCateC = $proCate['content'];
                    if (str_contains($productOptions[count($productOptions) - 1]->id, 'model')) {
                        if ($brandId) {
                            $proCateC = $proCateC[$proCate['type'] . 'brand'][$productOptions[count($productOptions) - 1]->id][$brandId]['options'];
                        } else {
                            continue;
                        }
                    } else {
                        if (!isset($proCateC[$productOptions[count($productOptions) - 1]->id])) {
                            continue;
                        }
                        $proCateC = $proCateC[$productOptions[count($productOptions) - 1]->id]['options'];
                    }
                    foreach ($proCateC as $key => $proCateValue) {
                        if ($proCateValue['value'] == $valueOptionProduct) {
                            $productOptions[count($productOptions) - 1]->value = $proCateValue['value'];
                            $productOptions[count($productOptions) - 1]->value_id = $proCateValue['id'];
                            if (str_contains($productOptions[count($productOptions) - 1]->id, 'brand')) {
                                $brandId = $productOptions[count($productOptions) - 1]->value_id;
                                $this->title = $proCateValue['value'];
                            }
                        }
                    }
                }
            }
        }
        return $productOptions;
    }

    public function getListProduct($offset)
    {
        $params = $offset >= 100 ? [
            'limit' => 100,
            'o' => $offset,
            'page' => 1,
            'cg' => 2000,
            'st' => 's,k',
            // 'protection_entitlement'=>'true',
            'key_param_included' => 'true'
        ] : [
            'limit' => 100,
            'page' => 1,
            'cg' => 2000,
            'st' => 's,k',
            // 'protection_entitlement'=>'true',
            'key_param_included' => 'true'
        ];
        $response = Http::withOptions([
            'debug' => true,
        ])->get(
            'https://gateway.chotot.com/v1/public/ad-listing',
            $params
        );
        return $response->json();
    }
    /**
     * $string is mixed area + region
     */
    public function createSlug($string, $delimiter = '-')
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );
        $replace = array(
            'a',
            'e',
            'i',
            'o',
            'u',
            'y',
            'd',
            'A',
            'E',
            'I',
            'O',
            'U',
            'Y',
            'D',
            '-',
        );
        $string = preg_replace($search, $replace, $string);
        $string = preg_replace('/(-)+/', $delimiter, $string);
        $string = strtolower($string);
        return $string;
    }
}
