<?

namespace App\Http\Services;

use App\Jobs\SendMessZaloJob;
use App\Models\Member;
use App\Scraper\chototScaper;
use Google_Client;
use Illuminate\Support\Facades\Http;

class ProductService
{
    /**
     */
    public function getListProduct()
    {
        $response = Http::get('https://gateway.chotot.com/v1/public/ad-listing', 
                        [
                            'limti' => 10,
                            'protection_entitlement' => true,
                            'page' => 1,
                            'cg' => 2010,
                            'st' => 's,k',
                            'key_param_included' => true
                        ]
                        );
        return $response->json();
    }

    public function getDetailProduct($productId, $relativePath)
    {
        $ct = new chototScaper($productId, $relativePath);
        return $ct->scrape();
    }
}

